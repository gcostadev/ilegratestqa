Projeto de exemplo, utilizado para realização da prova técnica da Ilegra.

Estou passando meu contato para o caso de surguir alguma dúvida ou até mesmo queiram me passar algumas dica.
email: gcostadev@gmail.com

Documentação Karatê DSL: https://github.com/intuit/karate#index

API de Testes (PokéAPI): https://pokeapi.co/

Pré-requisitos:

1. Java 8+
2. Maven

Passo a passo:

1. Clone o projeto
2. Abra o terminal, navegue para o diretório do projeto e execute os comandos abaixo para atualizar as dependências e executar os testes específicos:

    > mvn clean install
    
	Execução de todos os testes contido na feature
    > mvn test -Dtest=PokedexTestRunner

	Execução dos testes referente a consultas sobre pokemons contidos na feature
	> mvn test -Dtest=PokedexTestRunner -DfailNoTests=false -Dcucumber.options="--tags @DadosPokemons"

	Execução do teste referente a consulta sobre as frutas
	> mvn test -Dtest=PokedexTestRunner -DfailNoTests=false -Dcucumber.options='--tags @Berrys'

	Execução dos testes de exceção contidos na Feature
	> mvn test -Dtest=PokedexTestRunner -DfailNoTests=false -Dcucumber.options='--tags @TestException'


3. Após a execução do teste, será gerado um reporte em html. O caminho é indicado no console. Se preferir, acesse na pasta target/surefire-reports
4. Para abrir o projeto, você pode utilizar qualquer editor de texto ou IDE de sua preferência.
