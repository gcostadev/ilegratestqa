@PokeTest
Feature: Consulta aos dados que o Professor Carvalho nos disponibilizou via API/Rest.

#   API utilizada: https://pokeapi.co/api/v2/
#   Sugestão de tag para utilizar no runner: @PokeTest
#   Site da API: https://pokeapi.co/

# Challenges:

#   1. Teste da pesquisa para obter dados sobre o <pokemon> e suas respectivas características validando os dados dos campos do response afim de realizar estas consultas pela pokédex.	
#   2. Teste para validar os dados das evoluções do Bulbasuro por ID de evolução disponível na documentação da API, validando os dados dos campos obrigatórios do response.
#   3. Teste para validar a pesquisa por todos os pokémons disponíveis na base de dados da API, validando os dados dos campos obrigatórios do response.
#   4. Teste de pesquisa sobre frutas, tipos de 'Firmnesses' e 'Flavors' disponíveis na base  de dados da  API, validando os dados dos campos obrigatórios do response.
#   
#   Testes de Exceções: 
#   5. Teste para validar as exceções para as requisições de busca de dados na API com pokémons que não existem.
#   6. Teste de pesquisa sobre frutas que não existem na API do professor carvalho.

Background:
* url "https://pokeapi.co/api/v2/"
																																
@DadosPokemons
Scenario Outline: Teste da pesquisa para obter dados sobre o <pokemon> e suas respectivas características validando os dados dos campos do response afim de realizar estas consultas pela pokédex.

Given path 'pokemon/','<pokemon>'
When method get
Then status 200
And match response contains '#object'
And match response contains
  """
  {
  	id: '#number',
  	name: '#regex <pokemon>',
  	base_experience: '#number',
  	height: '#number',
  	location_area_encounters: '#string',
		order: '#number',
  	weight: '#number'
  }
  
  """
And match response.abilities contains '#object'
And match response.abilities[*] contains
	"""
  {
    "ability": '#object',
    is_hidden: '#boolean',
    slot: '#number'
	}
  
  """
And match response.abilities[*].ability contains '#object'  
And match response.abilities[*].ability contains
"""
  {
	 "name": '#string',
   "url": '#string'
  }
  
"""  
And match response.forms contains '#object'
And match response.forms[*] contains
	"""
	{
    name: '#string',
    url: '#string'
 	}
	
	"""
And match response.game_indices contains '#object'
And match response.game_indices[*].game_index contains '#number'
And match response.game_indices[*].version contains '#object'
	
And match response.game_indices[*].version contains
	"""
	{
    name: '#string',
	  url: '#string'
  }
	
	"""
	
And match response.moves[*].move contains '#object'
And match response.moves[*].version_group_details contains '#array'
And match response.moves[*].version_group_details[*].level_learned_at contains '#number'	
And match response.moves[*].version_group_details[*].move_learn_method contains '#object'	
And match response.moves[*].version_group_details[*].move_learn_method contains
	"""
	{
    name: '#string',
	  url: '#string'
  }
	
	"""
And match response.moves[*].version_group_details[*].version_group contains '#object'	 	
And match response.moves[*].version_group_details[*].version_group contains
	"""
	{
	  name: '#string',
	  url: '#string'
	}
	
	"""
And match response.species contains '#object'	 	
And match response.species contains
	"""
	{
	  name: '#regex <pokemon>',
	  url: '#string'
	}
	
	"""			 		
And match response.sprites contains '#object'	 	
And match response.stats contains '#object'	 	
And match response.stats[*].base_stat contains '#number'
And match response.stats[*].effort contains '#number'
And match response.stats[*].stat contains '#object'
And match response.stats[*].stat contains
	"""
	{
	  name: '#string',
	  url: '#string'
	}
	
	"""					
And match response.types contains '#object'
And match response.types[*].slot contains '#number'
And match response.types[*].type contains
	"""
	{
	  name: '#string',
	  url: '#string'
	}
	
	"""			
Examples:
	| pokemon    |
	| pikachu    |
  | charmander |				
	| charmeleon |
	| charizard  |
	| bulbasaur  |
	| ivysaur    |
	| venusaur   |
	| squirtle   |
	| wartortle  |
	| blastoise	 |
	| ditto      |						
  | mewtwo     |
  
@DadosPokemons
Scenario: Teste para validar os dados das evoluções do Bulbasuro por ID de evolução disponível na documentação da API, validando os dados dos campos obrigatórios do response.

Given path 'evolution-chain/1'
When method get
Then status 200
And match response contains '#object'	
And match response contains
  """
  {
  	id:'#number',
  	chain:'#object'
	}
  """   
And match response.chain contains
  """
  {
  	evolves_to: '#array',
    species: {
      name: '#string',
      url: '#string'
    }
	}
  """    
  
  
@DadosPokemons
Scenario: Teste para validar a pesquisa por todos os pokémons disponíveis na base de dados da API, validando os dados dos campos obrigatórios do response.

Given path 'pokemon'
When method get
Then status 200
And match response contains '#object'	
And match response.results[*] contains
  """
  {
    name: '#string',
    url: '#string'
  }
  """
  
@Berrys
Scenario: Teste de pesquisa sobre frutas, tipos de 'Firmnesses' e 'Flavors' disponíveis na base  de dados da  API, validando os dados dos campos obrigatórios do response.

Given path 'berry'
When method get
Then status 200
And match response contains '#object'	
And match response.results[*] contains
  """
  {
    name: '#string',
    url: '#string'
  }
  """  
Given path 'berry-flavor/'
When method get
Then status 200
And match response contains '#object'	
And match response.results[*] contains
  """
  {
    name: '#string',
    url: '#string'
  }
  """  
Given path 'berry-firmness/'
When method get
Then status 200
And match response contains '#object'	
And match response.results[*] contains
  """
  {
    name: '#string',
    url: '#string'
  }
  """    
  
@TestException
Scenario Outline: Teste para validar as exceções para as requisições de busca de dados na API com pokémons que não existem, a API deve retornar código 404.

Given path 'pokemon/','<pokemon>'
When method get
Then status 404

Examples:
	| pokemon      |
	| batman       |
	| homem-aranha |				
	| papaleguas   |
	| ikki         |
	| mrrobot      |
	| bobesponja   |
	| capamerica   |
	| hulk         |
	| thor         |
	| scarllet     |
	
@TestException
Scenario: Teste de pesquisa sobre frutas que não existem na API disponibilizada pelo professor carvalho, a API deve retornar código 404.

Given path 'berry', 1234
When method get
Then status 404
